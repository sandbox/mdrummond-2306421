# Require any additional compass plugins here.
require 'compass/import-once/activate'
require 'compass-normalize'
require 'toolkit'
require 'breakpoint'
require 'susy'
require 'modular-scale'
require 'color-schemer'
require 'sassy-buttons'

# Default to development if environment is not set.
saved = environment
if (environment.nil?)
  environment = :development
else
  environment = saved
end

# In development, turn on the FireSass-compatible debug_info by default.
firesass = (environment == :development) ? true : false

# Location of the theme's resources.
css_dir = "css"
sass_dir = "sass"
images_dir = "images"
javascripts_dir = "js"

output_style = (environment == :development) ? :expanded : :compressed
relative_assets = true

# Conditionally enable line comments when in development mode.
line_comments = (environment == :development) ? true : false

# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass sass scss && rm -rf sass && mv scss sass

# Pass options to sass. For development, we turn on the FireSass-compatible
# debug_info if the firesass config variable above is true.
sass_options = (environment == :development && firesass == true) ? {:debug_info => true} : {}
