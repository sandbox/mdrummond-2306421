<?php
/**
 * Implements hook_form_system_theme_settings_alter() function.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
 
function atomic_form_system_theme_settings_alter(&$form, $form_state) {

  //////////////////////////////
  // Miscelaneous
  //////////////////////////////

  $form['typography'] = array(
    '#type' => 'fieldset',
    '#title' => t('Typography'),
    '#description' => t('Typography settings for your theme.'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE
  );

  $form['typography']['atomic_typekit_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Typekit Kit ID'),
    '#default_value' => theme_get_setting('atomic_typekit_id'),
    '#size' => 7,
    '#maxlength' => 7,
    '#description' => t('If you are using <a href="!link" target="_blank">Typekit</a> to serve webfonts, put your Typekit Kit ID here.', array('!link' => 'https://typekit.com/')),
  );

  $form['typography']['atomic_typekit_advanced'] = array(
    '#type' => 'boolean',
    '#title' => t('Advanced Typekit Embed'),
    '#default_value' => theme_get_setting('atomic_typekit_advanced'),
    '#description' => t('This turns on the <a href="!link" target="_blank">advanced Typekit embed</a>.', array('!link' => 'http://help.typekit.com/customer/portal/articles/649336-embed-code')),
  );

}